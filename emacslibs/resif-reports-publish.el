;;; resif-reports-publish --- Publish the reports
;;; Commentary:
;;
;;
(require 'ox-publish)

;; Pour que les résultats "inline" ne soient pas formatés :
(setq org-babel-inline-result-wrap "%s")


(setq
 org-publish-project-alist
  '(
    ("org-notes"
     :base-directory "./reports/"
     :base-extension "org"
     :exclude "static/*"
     :publishing-directory "./public/"
     :recursive t
     :publishing-function org-html-publish-to-html
     :auto-sitemap t                ; Generate sitemap.org automagically...
     :sitemap-filename "sitemap.org"
     :sitemap-sort-files anti-chronologically
     :sitemap-style list
     :sitemap-title ""
     )
     ("org-static"
      :base-directory "./reports/"
      :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
      :publishing-directory "./public/"
      :recursive t
      :publishing-function org-publish-attachment
      )
     ("all" :components ("org-notes" "org-static"))))

(provide 'emacs-run)
;;; resif-reports-publish ends here
