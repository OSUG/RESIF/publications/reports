% Created 2025-01-08 mer. 17:28
% Intended LaTeX compiler: lualatex
\documentclass[a4paper,11pt]{article}
\usepackage{amsmath}
\usepackage{fontspec}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{polyglossia}
\setmainlanguage{french}
\usepackage{lmodern}
\usepackage{fontspec}
\usepackage{xcolor}
\usepackage{booktabs}

\usepackage{orcidlink}
\usepackage{titling}
\usepackage{array}
\usepackage[a4paper,margin=2.5cm]{geometry}
\usepackage{fancyhdr}
\usepackage{xurl}
\usepackage[tikz]{bclogo}
\setmainfont{Latin Modern Sans}
\setsansfont{Latin Modern Sans}
\renewcommand{\headrulewidth}{0.4pt}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\nouppercase{\slshape\leftmark}}
\fancyhead[R]{\includegraphics[height=7mm]{resif_logo.png}}
\fancyfoot[C]{\thepage}
\PassOptionsToPackage{hyperref,x11names}{xcolor}
\definecolor{electricblue}{HTML}{05ADF3}
\hypersetup{breaklinks, linktoc=all, colorlinks, citecolor=electricblue,filecolor=electricblue,linkcolor=electricblue,urlcolor=electricblue}
\author{Jonathan Schaeffer \orcidlink{0000-0002-2447-7394}\\ Helle Pedersen \orcidlink{0000-0003-2936-8047}}
\usetheme{default}
\setcounter{secnumdepth}{2}
\author{Jonathan Schaeffer,  Helle Pedersen, Équipe du centre de données sismologique <sismo-help@resif.fr>}
\date{\today}
\title{Rapport annuel des services du centre de données sismologiques Epos-France}
\hypersetup{
 pdfauthor={Jonathan Schaeffer,  Helle Pedersen, Équipe du centre de données sismologique <sismo-help@resif.fr>},
 pdftitle={Rapport annuel des services du centre de données sismologiques Epos-France},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={<a href="https://www.gnu.org/software/emacs/">Emacs</a> 27.2 (<a href="https://orgmode.org">Org</a> mode 9.6)}, 
 pdflang={French}}
\begin{document}

\maketitle
\begin{frame}{Outline}
\setcounter{tocdepth}{2}
\tableofcontents
\end{frame}

Ce document est à usage interne du centre de données Résif-DC\footnote{Résif Seismological Data Portal; editing status 2023-02-22; re3data.org - Registry of Research Data Repositories. \url{http://doi.org/10.17616/R37Q06}}. Cependant les informations et figures ne sont pas confidentielles et peuvent être réutilisées sous les termes de la licence \href{https://creativecommons.org/licenses/by/4.0/deed.fr}{Creative Commons Attribution 4.0}.
\begin{frame}[label={sec:org555ed39}]{Résumé}
La disponibilité des services reste excellente. La migration des services dans un cluster kubernetes a apporté une amélioration de la résilience des services et une capacité à l'auto-réparation après une panne ou un dysfonctionnement.
Les indicateurs de l'année 2024 montrent principalement un accroissement fort de la distribution des données. Cela peut s'expliquer par l'amélioration des performances du service de distribution au début de l'année 2024.
On a aussi constaté un fort accroissement des requêtes sur le service \og availability\fg{}, suite logique de son adoption très large au sein de la fédération européenne EIDA.
L'indicateur du nombre d'IP distinctes s'emballe en 2024. On ne peut pas conclure à une augmentation équivalente du nombre d'utilisateurs des données, mais plutôt un changement de fonctionnement des clients qui utilisent davantage de services cloud, augmentant mécaniquement le nombre d'IP distinctes. Cela montre aussi la pertinence de mettre en place un nouveau système d'authentification au sein de la FDSN afin de mieux connaître les utilisateurs des données.
\end{frame}
\begin{frame}[label={sec:org0f2e5c4}]{Disponibilité des services}
\begin{block}{Indicateurs}
Disponibilité des services d'accès aux données/métadonnées.

Le taux de disponibilité est mesuré par un service externe (uptime robot) et montre une excellente disponibilité globale des services.

\begin{table}[htbp]
\caption{\label{tab-dispo}taux de disponibilité des services pour l'année 2024}
\centering
\begin{tabular}{lrl}
service & disponibilité & commentaire\\
\hline
fdsnws - availability & 99.862 & \\
fdsnws - dataselect & 99.986 & \\
fdsnws - event & 99.934 & \\
fdsnws - station & 99.997 & \\
rtserve.resif.fr & 99.998 & \\
\end{tabular}
\end{table}

\begin{quote}
\begin{itemize}
\item 0.05\% équivaut environ à 4h d'arrêt
\item 1\%    équivaut à 3.5 jours d'arrêt
\end{itemize}
\end{quote}

\clearpage
\newpage
\end{block}
\end{frame}
\begin{frame}[label={sec:org9af5dbd},fragile]{Services de distribution de données}
 \begin{verse}
En 2024, le centre de données:

- a distribué 170.7TO de données\\
- a traité environ 118.53 millions de requêtes valides sur la donnée\\
- a traité environ 140.8 millions de requêtes valides sur la métadonnée\\
- a servi 23164 adresses IP distinctes.\\
- a émis l'équivalent de 18.0 tonnes de CO2\\
\end{verse}
\begin{block}{Obtenir le flux des données temps réel \texttt{seedlink}}
Le service temps réel permet de récupérer un flux de données au format miniSEED.
Documentation utilisateur : \url{https://seismology.resif.fr/real-time-seedlink/}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-seedlink}Nombre de clients unique et volume de données livrées par année}
\centering
\begin{tabular}{rrr}
year & unique IP & volume (TB)\\
\hline
2013 & 50 & 4.7\\
2014 & 175 & 23.8\\
2015 & 279 & 35.6\\
2016 & 340 & 40.3\\
2017 & 367 & 40.5\\
2018 & 479 & 36.3\\
2019 & 1591 & 27.4\\
2020 & 1234 & 30.7\\
2021 & 867 & 27.8\\
2022 & 634 & 38.0\\
2023 & 1668 & 30.5\\
2024 & 10345 & 76.0\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_seedklink.png}
\caption{\label{fig:seedlink}Nombre de clients unique et volume de données livrées par année. Graphique du tableau \ref{tab-seedlink}}
\end{figure}

\clearpage
\end{block}
\end{block}
\begin{block}{Obtenir des données \texttt{fdsnws-dataselect}}
Le webservice FDSN dataselect permet d'obtenir les données de manière standard, au format miniSEED.
Documentation utilisateur: \url{http://ws.resif.fr/fdsnws/dataselect/1}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-wsdataselect}Décompte des requêtes au webservice dataselect en million. \og total req\fg{} = \og ok req\fg{} + \og no data\fg{} + \og internal errors\fg{} ; \og internal errors\fg{}: Tous les codes retours de type 400 ou 500}
\centering
\begin{tabular}{rrrrrr}
year & total req & ok req & no data & bad req & internal errors\\
\hline
2019 & 28.98 & 15.44 & 8.70 & 4.74 & 0.10\\
2020 & 80.70 & 47.05 & 17.06 & 16.49 & 0.10\\
2021 & 78.58 & 38.68 & 22.19 & 16.68 & 1.02\\
2022 & 78.35 & 39.13 & 28.46 & 10.36 & 0.39\\
2023 & 89.88 & 54.84 & 20.89 & 14.02 & 0.13\\
2024 & 163.08 & 80.39 & 54.31 & 24.89 & 3.48\\
\end{tabular}
\end{table}


\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_wsdataselect.png}
\caption{\label{fig:wsdataselect}Graphique du tableau \ref{tab-wsdataselect}.}
\end{figure}

\begin{table}[htbp]
\caption{\label{tab-dataselect}Nombre de requêtes réussies (millions) sur la donnée, nombre d'IP uniques et volumétrie en téraoctets. Ces chiffres concernent uniquement les requêtes réussies.}
\centering
\begin{tabular}{rrrr}
year & requests (millions) & unique IP & volume (TB)\\
\hline
2013 & 0.00 & 6 & 0.0\\
2014 & 0.86 & 533 & 8.5\\
2015 & 3.01 & 603 & 7.7\\
2016 & 4.42 & 322 & 5.4\\
2017 & 8.93 & 638 & 14.2\\
2018 & 21.33 & 1238 & 50.1\\
2019 & 12.90 & 1595 & 26.0\\
2020 & 33.72 & 2645 & 51.3\\
2021 & 26.25 & 1448 & 85.5\\
2022 & 33.46 & 3887 & 114.4\\
2023 & 64.73 & 7881 & 68.2\\
2024 & 118.53 & 13894 & 170.7\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_dataselect_vol.png}
\caption{\label{fig:dataselectvol}Nombre de clients uniques et volume de données livrées par dataselect. Graphique du tableau \ref{tab-dataselect}.}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_dataselect_req.png}
\caption{\label{fig:dataselectvol2}Nombre de clients uniques et nombre de requêtes. Graphique du tableau \ref{tab-dataselect}.}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateur_dataselect_chloropleth.png}
\caption{\label{fig:dataselect_chloropleth}Volume de données distribuées dans chaque pays pour l'année 2024}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateur_dataselect_req_chloropleth.png}
\caption{\label{fig:dataselect_req_chloropleth}Origine des requêtes par pays pour l'année 2024}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateur_dataselect_treemap.png}
\caption{\label{fig:dataselect_treemap}Répartition de la quantité de données livrées en 2024 par pays}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateur_dataselect_reqs_treemap.png}
\caption{\label{fig:dataselect_req_treemap}Répartition des requêtes dataselect en 2024 par pays}
\end{figure}

Dans la figure \ref{fig:dataselect_req_treemap}, les nombreuses requêtes issues de Nouvelle Zélande  proviennent de 3 adresses IP distinctes, avec une prédominance de client Swarm, utilisé pour récupérer de la donnée quasi temps réel sur les réseaux ND et PF. Cette activité a duré tout au long de l'année, mais semble s'être arrêtée fin décembre. On peut imaginer qu'il s'agit d'un système de surveillance. Une des IP appartient à l'université de Wellington, mais l'IP réalisant 80\% des requêtes ne porte pas d'information sur son propriétaire (IP d'opérateur télécom).

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateur_dataselect_clients_treemap.png}
\caption{\label{fig:dataselect_clients_treemap}Répartition des clients dataselect en 2024 par pays}
\end{figure}

La figure \ref{fig:dataselect_clients_treemap} montre un nombre très important de clients venant de Singapour. Une exploration des données de l'activité montre que l'on reçoit des requêtes issues de l'université de NanYang tout au long de l'année, avec parfois un pic d'activité. 3 ou 4 adresses IP sont largement prédominantes (75\% des requêtes issues de 6 adresses IP). On observe qu'un très faible nombre de requêtes sont issues de nombreuses adresses IP. Il est difficile de proposer un scénario expliquant cette activité, mais cela pourrait venir d'un cluster privé par exemple.

\clearpage
\end{block}
\end{block}
\begin{block}{Obtenir des métadonnées \texttt{fdsnws-station}}
Le webservice \texttt{station} permet aux usagers de consulter les métadonnées.
Documentation : \url{http://ws.resif.fr/fdsnws/station/1}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-stationreq}Nombre de requêtes réussies (millions), nombre de clients uniques, volume des données envoyées}
\centering
\begin{tabular}{rrrrr}
year & all requests (millions) & ok requests & unique IP & volume (GO)\\
\hline
2019 & 11.2 & 9.0 & 2619 & 1222.4\\
2020 & 21.9 & 16.0 & 5502 & 600.0\\
2021 & 43.2 & 36.0 & 7888 & 1161.1\\
2022 & 89.2 & 81.0 & 10605 & 2880.6\\
2023 & 60.4 & 39.0 & 14823 & 5788.4\\
2024 & 140.8 & 103.0 & 33900 & 7939.2\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_station_req.png}
\caption{\label{fig:stationreq}Représentation graphique du tableau \ref{tab-stationreq}}
\end{figure}

\clearpage
\end{block}
\end{block}
\begin{block}{Récupérer des jeux de données par \texttt{rsync}}
Données récupérées par le portail rsync concernant des réseaux.
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-rsync}Données envoyées par protocole rsync, nombre de requêtes et nombre de clients uniques.}
\centering
\begin{tabular}{rrrr}
year & Sent (GB) & requests & clients\\
\hline
2015 & 0 & 1 & 1\\
2016 & 200 & 12 & 2\\
2017 & 368 & 47585 & 14\\
2018 & 3736 & 166562 & 23\\
2019 & 5684 & 175245 & 49\\
2020 & 1383 & 19034 & 19\\
2021 & 8 & 1987 & 4\\
2022 & 3328 & 5745 & 14\\
2023 & 148 & 2827 & 2\\
2024 & 5939 & 1670 & 12\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_rsync_vol.png}
\caption{\label{fig:rsync1}Volumes envoyés. Représentation de la première colonne du tableau \ref{tab-rsync}}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_rsync_req.png}
\caption{\label{fig:rsync2}Nombre de clients et de requêtes. Représentation du tableau \ref{tab-rsync}}
\end{figure}

\clearpage
\end{block}
\end{block}
\begin{block}{Connaître la disponibilité \texttt{fdsnws-availability}}
Webservice permettant de consulter la disponibilité des données.
Documentation: \url{http://ws.resif.fr/fdsnws/availability/1}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-availability}Nombre de requêtes et d'IP uniques pour le webservice availability.}
\centering
\begin{tabular}{rrr}
year & requests & unique IP\\
\hline
2019 & 2057 & 125\\
2020 & 8832 & 291\\
2021 & 44133 & 1288\\
2022 & 69846 & 2267\\
2023 & 808616 & 3235\\
2024 & 1626991 & 5172\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_availability.png}
\caption{\label{fig:availability}Représentation du tableau \ref{tab-availability}}
\end{figure}

\clearpage
\end{block}
\end{block}
\begin{block}{Métriques qualité EIDA \texttt{eidaws-wfcatalog}}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-wfcatalog}Nombre de requêtes et d'IP uniques pour le webservice wfcatalog.}
\centering
\begin{tabular}{rrr}
year & requests & unique IP\\
\hline
2019 & 60351 & 82\\
2020 & 213785 & 379\\
2021 & 487573 & 520\\
2022 & 170726 & 2060\\
2023 & 6453526 & 2124\\
2024 & 8695541 & 2925\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_wfcatalog.png}
\caption{\label{fig:wfcatalog}Représentation du tableau \ref{tab-wfcatalog}}
\end{figure}

\clearpage
\end{block}
\end{block}
\begin{block}{Consulter des séries temporelles prétraitées \texttt{timeseries} et \texttt{timeseriesplot}}
Ce service permet de consulter les séries temporelles sous différents formats, en particulier sous forme graphique.
Documentation : \url{https://ws.resif.fr/resifws/timeseries/} et \url{http://ws.resif.fr/resifws/timeseriesplot}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-timeseries}Nombre de requêtes et IP uniques sur les webservices timeseries et timeseriesplot.}
\centering
\begin{tabular}{rrr}
Year & Requests & unique IP\\
\hline
2019 & 24681 & 929\\
2020 & 42936 & 901\\
2021 & 8052413 & 1314\\
2022 & 11127434 & 1497\\
2023 & 9634622 & 2249\\
2024 & 8579808 & 8908\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_timeseries.png}
\caption{\label{fig:timeseries}Représentation du tableau \ref{tab-timeseries}}
\end{figure}

\clearpage
\end{block}
\end{block}
\begin{block}{Consulter la qualité des données avec des PSD}
Ce service a été ouvert aux utilisateurs en octobre 2022: \url{https://ws.resif.fr/resifws/seedpsd/1/}
\begin{block}{indicateurs}
\begin{table}[htbp]
\caption{\label{tab-seedpsd}Nombre de requêtes et IP uniques sur le webservices seedpsd}
\centering
\begin{tabular}{rrr}
Year & Requests & unique IP\\
\hline
2022 & 288 & 56\\
2023 & 14482 & 1269\\
2024 & 14875 & 2181\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_seedpsd.png}
\caption{\label{fig:seedpsd}Représentation graphique du tableau \ref{tab-seedpsd}}
\end{figure}
\end{block}
\end{block}
\begin{block}{Obtenir des données d'événement pré-assemblés \texttt{assembleddata}}
Permet d'obtenir des donnés d'événements pré-assemblés à partir des données du RAP.
Documentation: \url{http://ws.resif.fr/resifsi/assembleddata/1}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-assembleddata}Nombre de requêtes et IP uniques sur le webservice assembleddata}
\centering
\begin{tabular}{rrrr}
Année & Requêtes & unique IP & volume (GO)\\
\hline
2019 & 56 & 26 & 0.3\\
2020 & 331 & 120 & 0.1\\
2021 & 66 & 18 & 0.1\\
2022 & 1496 & 19 & 1.2\\
2023 & 1276 & 238 & 0.0\\
2024 & 16 & 5 & 0.0\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_assembleddata.png}
\caption{\label{fig:assembleddata}Représentation du tableau \ref{tab-assembleddata}}
\end{figure}

\clearpage
\end{block}
\end{block}
\begin{block}{Obtenir des données Large-N au format miniSEED \texttt{ph5-dataselect}}
Ce service permet de distribuer en miniSEED la donnée stockée au format PH5.
Documentation: \url{http://ws-ph5.resif.fr/fdsnws/ph5-dataselect/1}

\alert{Note}:les service est déclaré dans obspy depuis sa version 1.3.0 en mars 2022.
\begin{block}{Indicateurs}
\begin{table}[htbp]
\label{indicateur_ph5_dataselect}
\centering
\begin{tabular}{rrrr}
Année & requêtes & unique IP & volume (GO)\\
\hline
2020 & 6 & 5 & 0.0\\
2021 & 17807 & 412 & 48.1\\
2022 & 2525270 & 1794 & 37.6\\
2023 & 1311929 & 3880 & 4.6\\
2024 & 453710 & 3423 & 706.1\\
\end{tabular}
\end{table}

\begin{center}
\includegraphics[width=.9\linewidth]{./img/indicateurs_ph5_dataselect.png}
\label{plot_ph5_dataselect}
\end{center}

\clearpage
\end{block}
\end{block}
\begin{block}{Consulter la disponibilité des données Large-N \texttt{ph5-availability}}
Documentation: \url{https://ws.resif.fr/resifws/ph5-availability/1/}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-ph5availability}requêtes au webservice ph5-availability}
\centering
\begin{tabular}{rrr}
Année & requêtes & unique IP\\
\hline
2020 & 9 & 7\\
2021 & 1082 & 298\\
2022 & 6356 & 546\\
2023 & 2223 & 434\\
2024 & 2072 & 581\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_ph5_availability.png}
\caption{\label{plot_ph5_availability}Graphique du tableau \ref{tab-ph5availability}}
\end{figure}

\clearpage
\end{block}
\end{block}
\begin{block}{Autres webservices}
\begin{itemize}
\item resp : \url{http://ws.resif.fr/resifws/resp/1}
\item sacpz : \url{http://ws.resif.fr/resifws/sacpz/1}
\item evalresp : \url{http://ws/resif.fr/resifws/evalresp/1}
\end{itemize}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-ws_resp}Indicateurs pour le webservice resp}
\centering
\begin{tabular}{rrr}
Année & requêtes & unique IP\\
\hline
2020 & 69103 & 30\\
2021 & 14625 & 409\\
2022 & 23536 & 605\\
2023 & 26581 & 557\\
2024 & 11208 & 1434\\
\end{tabular}
\end{table}

\begin{table}[htbp]
\caption{\label{tab-ws_sacpz}Indicateurs pour le webservice sacpz}
\centering
\begin{tabular}{rrr}
Année & requêtes & unique IP\\
\hline
2020 & 3 & 3\\
2021 & 1738 & 384\\
2022 & 5972 & 546\\
2023 & 16215 & 489\\
2024 & 10541 & 1266\\
\end{tabular}
\end{table}

\begin{table}[htbp]
\caption{\label{tab-ws_evalresp}Indicateurs pour le webservice evalresp}
\centering
\begin{tabular}{rrr}
Année & requêtes & unique IP\\
\hline
2020 & 3 & 3\\
2021 & 2423 & 460\\
2022 & 4749 & 744\\
2023 & 3640 & 686\\
2024 & 6727 & 1499\\
\end{tabular}
\end{table}

\clearpage
\end{block}
\end{block}
\end{frame}
\begin{frame}[label={sec:orge471271},fragile]{Services d'intégration et de gestion des données}
 \begin{block}{Création de nouveaux réseaux temporaires}
Nombre de nouveaux réseaux temporaires ajoutés au centre de données chaque année.
\begin{block}{Indicateurs}
Le tableau \ref{tab-temp_net} montre la répartition des réseaux temporaire par date de début déclaré à la FDSN.
La répartition par année n'est pas exacte car elle ne correspond pas à sa date de création au centre de données.

\begin{table}[htbp]
\caption{\label{tab-temp_net}Nombre de réseaux temporaires déclarés par année}
\centering
\begin{tabular}{rr}
year & count\\
\hline
1998 & 1\\
1999 & 1\\
2000 & 1\\
2001 & 3\\
2003 & 4\\
2004 & 2\\
2007 & 5\\
2008 & 5\\
2009 & 5\\
2010 & 5\\
2011 & 2\\
2012 & 1\\
2013 & 1\\
2014 & 5\\
2015 & 6\\
2016 & 4\\
2017 & 3\\
2018 & 12\\
2019 & 8\\
2020 & 6\\
2021 & 6\\
2022 & 5\\
2023 & 3\\
2024 & 4\\
\end{tabular}
\end{table}

\clearpage
\end{block}
\end{block}
\begin{block}{Soumettre de la donnée et de la métadonnée validées \texttt{ResifDataTransfer}}
Ce service permet aux producteurs de donnée et de métadonnée de soumettre leurs produits au centre de données.
Documentation : \url{https://gitlab.com/resif/resif-data-transfer/-/blob/master/MANUAL\_fr.md}
\begin{block}{Indicateurs}
Les tableaux \ref{tab-mseed}, \ref{tab-ph5}, \ref{tab-metadata} montrent les historiques d'indicateurs pour ce service.

Les figures \ref{fig:mseed}, \ref{fig:ph5} et \ref{fig:metadata} reprennent les chiffres sous forme de graphiques.
\begin{table}[htbp]
\caption{\label{tab-mseed}indicateurs pour les transactions de données miniSEED}
\centering
\begin{tabular}{rrr}
Année & transactions mSEED & volume (GO)\\
\hline
2014 & 700 & 11638\\
2015 & 1592 & 11168\\
2016 & 2186 & 13969\\
2017 & 2601 & 12955\\
2018 & 1467 & 17001\\
2019 & 1966 & 17951\\
2020 & 1333 & 7730\\
2021 & 1976 & 10264\\
2022 & 3146 & 14680\\
2023 & 3609 & 14662\\
2024 & 3448 & 18413\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_transactions_mseed.png}
\caption{\label{fig:mseed}Transactions miniSEED, selon le tableau \ref{tab-mseed}}
\end{figure}

\begin{table}[htbp]
\caption{\label{tab-ph5}indicateurs pour les transactions de données PH5}
\centering
\begin{tabular}{rrr}
Année & transactions PH5 & volume (GO)\\
\hline
2020 & 10 & 3549\\
2021 & 2 & 424\\
2022 & 3 & 507\\
2023 & 1 & 279\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_transactions_ph5.png}
\caption{\label{fig:ph5}Transactions PH5, selon le tableau \ref{tab-ph5}}
\end{figure}

\begin{table}[htbp]
\caption{\label{tab-metadata}indicateurs d'intégration de métadonnées (stationXML et dataless)}
\centering
\begin{tabular}{rr}
Année & transactions métadonnée\\
\hline
2015 & 975\\
2016 & 1550\\
2017 & 918\\
2018 & 2161\\
2019 & 2698\\
2020 & 2824\\
2021 & 1067\\
2022 & 1058\\
2023 & 1344\\
2024 & 1117\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_transactions_stationxml.png}
\caption{\label{fig:metadata}Transactions de métadonnées, selon le tableau \ref{tab-metadata}}
\end{figure}

\clearpage
\end{block}
\end{block}
\begin{block}{Connaître l'état d'une transaction d'intégration (ws \texttt{transaction})}
L'état d'une transaction peut être consulté :
\begin{itemize}
\item par requête sur le module rsync \texttt{TRANSACTION\_XML}
\item par requête au webservice transaction.
\end{itemize}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-rsynctransaction}Nombre de téléchargement sur le module rsync \texttt{TRANSACTION\_XML}}
\centering
\begin{tabular}{rrr}
Year & requests & clients\\
\hline
2022 & 19318 & 6\\
2023 & 11002 & 6\\
2024 & 10961 & 7\\
\end{tabular}
\end{table}

\begin{table}[htbp]
\caption{\label{tab-wstransaction}Nombre d'accès au webservice transaction}
\centering
\begin{tabular}{rrr}
Année & requêtes & unique IP\\
\hline
2019 & 88 & 11\\
2020 & 695 & 70\\
2021 & 1070 & 219\\
2022 & 768 & 192\\
2023 & 731 & 195\\
2024 & 869 & 378\\
\end{tabular}
\end{table}

\clearpage
\end{block}
\end{block}
\begin{block}{Lister les fichiers orphelins (ws \texttt{orphanfile})}
Les fichiers orphelins ne sont décrits par aucune métadonnée. En conséquence de quoi, ils ne peuvent être distribués par le centre de données.
Ce service permet aux producteurs de données et de métadonnées d'obtenir une liste des fichiers orphelins.

Documentation: \url{http://ws.resif.fr/resifsi/orphanfile/1}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-orphan}Nombre d'accès au webservice orphanfile}
\centering
\begin{tabular}{rrr}
Année & requêtes & unique IP\\
\hline
2019 & 30 & 7\\
2020 & 157 & 22\\
\end{tabular}
\end{table}

\clearpage
\newpage
\end{block}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgf438721}]{Autres services}
\begin{block}{Gestion des DOI}
Résif-DC assure un service d'enregistrement et de maintenance des DOI pour les réseaux sismologiques.

Documentation: Pour enregistrer un DOI, il faut en faire la demande par ticket \url{mailto:sismo-help@resif.fr}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-doi}Nombre de DOI créés par an}
\centering
\begin{tabular}{rr}
year & DOIs\\
2024 & 15\\
2023 & 5\\
2022 & 6\\
2021 & 10\\
2020 & 5\\
2019 & 4\\
2018 & 26\\
2017 & 2\\
2016 & 5\\
2015 & 8\\
\end{tabular}
\end{table}

\clearpage
\end{block}
\end{block}
\begin{block}{Assistance aux utilisateurs}
Plusieurs interfaces d'assistance existent en fonction du périmètres et du statut de l'utilisateur
\begin{itemize}
\item Usersupport EIDA
\item Helpdesk Résif-DC (par mail \url{mailto:sismo-help@resif.fr}). Ce helpdesk a migré en juin 2021 vers la plateforme \url{https://gitlab.com/resif/sismo-help}
\end{itemize}
Les 164 demandes se répartissent en : 102 pour GLPI et 62 pour gitlab
\begin{block}{Indicateurs}
Le tableau \ref{tab-glpi} reprend l'historique des demandes soumises au helpdesk.
\begin{table}[htbp]
\caption{\label{tab-glpi}Nombre de demandes extérieures par an sur le helpdesk UGA}
\centering
\begin{tabular}{rr}
Year & Tickets\\
\hline
2021 & 164\\
2020 & 138\\
2019 & 139\\
2018 & 140\\
\end{tabular}
\end{table}

Le tableau \ref{tab-servicedesk} reprend l'historique des demandes soumises au nouveau servicedesk :

\begin{table}[htbp]
\caption{\label{tab-servicedesk}Demandes par année sur le servicedesk gitlab.com}
\centering
\begin{tabular}{rr}
Year & Tickets\\
\hline
2022 & 89\\
2023 & 34\\
2024 & 25\\
\end{tabular}
\end{table}

\clearpage
\end{block}
\end{block}
\begin{block}{Portail web de RESIF-DC seismology.resif.fr}
Le portail web permet d'accéder à de la documentation, des informations riches et des produits dérivés calculés au centre de données.
\url{https://seismology.resif.fr}

\begin{center}
\includegraphics[width=.9\linewidth]{img/indicateurs_piwik.png}
\end{center}
\begin{block}{Indicateurs}
Consultations annuelles du site web
\end{block}
\end{block}
\begin{block}{Sauvegarde de données scientifiques}
Certaines données, en particulier issues du RENAG sont sauvegardées au centre de données.
\begin{block}{Indicateur}
Volumétrie sauvegardée : 15To
\clearpage
\end{block}
\end{block}
\begin{block}{Consulter les statistiques sur les données hébergées}
Le webservice statistiques permet d'exposer aux utilisateurs des chiffres concernant les volumes de données et le nombre de requêtes.
Documentation: \url{http://ws.resif.fr/resifws/statistics/1}
\begin{block}{Indicateurs}
\begin{table}[htbp]
\caption{\label{tab-wsstats}Nombre de requêtes et IP uniques sur les webservices statistics}
\centering
\begin{tabular}{rrr}
Year & Requests & unique IP\\
\hline
2020 & 1456 & 39\\
2021 & 54767 & 719\\
2022 & 23875 & 770\\
2023 & 37878 & 1031\\
2024 & 26605 & 1646\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_wsstats.png}
\caption{\label{fig:wsstats}Représentation du tableau \ref{tab-wsstats}}
\end{figure}

\clearpage
\newpage
\end{block}
\end{block}
\end{frame}
\begin{frame}[label={sec:orgb75faf0},fragile]{Impact environnemental}
 Répartition globale :
\begin{table}[htbp]
\caption{\label{impact_co2}kg eqCO\textsubscript{2} pour les différentes activités Résif}
\centering
\begin{tabular}{rrrrrrrr}
Année & Serveurs ϕ & Serveurs Virt. & Stockage & Distrib. données & Déplacements & Postes de travail & Total\\
\hline
2021 & 2001.4376 & 302.4 & 11370. & 162 & 2764 & 900 & 17499.838\\
2022 & 1179.6416 & 420. & 14136.7 & 219 & 2013 & 900 & 18868.342\\
2023 & 1200.8096 & 386.4 & 11370. & 141 & 1944 & 900 & 15942.210\\
2024 & 1133.0464 & 352.8 & 12810.2 & 354 & 2571 & 900 & 18121.046\\
\end{tabular}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/indicateurs_co2.png}
\caption{\label{fig:plot_impact}CO\textsubscript{2} emissions per year.}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{img/co2.png}
\caption{\label{fig:co2_pie}CO\textsubscript{2} emissions for last year}
\end{figure}
\begin{block}{Infrastructure}
\begin{block}{Espaces de stockage}
Le stockage SUMMER est évalué à 37,9g de CO\textsubscript{2} équivalent par GigaOctet (selon \href{https://hal-cnrs.archives-ouvertes.fr/hal-03573790}{l'étude EcoInfo/Gricad 2021}\footnote{Guillaume Charret, Alexis Arnaud, Francoise Berthoud, Bruno Bzeznik, Anthony Defize, et al.. Estimation de l'empreinte carbone du stockage de données. [Rapport de recherche] CNRS - GRICAD. 2020. ⟨hal-03573790⟩ \url{https://cnrs.hal.science/hal-03573790}}).

Nous réservons actuellement 200TB pour la partie \og Centre de Données\fg{} et 100TB pour les nœuds A RAP et SISMOB.

\begin{table}[htbp]
\caption{\label{tab-co2e-stockage}Impact énergétique et environnemental du stockage par année}
\centering
\begin{tabular}{rrrr}
Année & Volume total (Go) & CO\textsubscript{2} eq (kg) & Consommation (kWh)\\
\hline
2021 & 300000 & 11370. & 105.27778\\
2022 & 373000 & 14136.7 & 130.89537\\
2023 & 300000 & 11370. & 105.27778\\
2024 & 338000 & 12810.2 & 118.61296\\
\end{tabular}
\end{table}
\end{block}
\begin{block}{Serveurs physiques}
La consommation électrique moyenne des serveurs physiques est calculée à partir des mesures sur leurs interfaces de contrôle. Cette mesure est fournie par le serveur en grandeur cumulée depuis une date de départ et doit donc être moyennée sur une année.

Nous appliquons un PUE de 1.4 correspondant à la valeur estimée dans l'étude EcoInfo/Gricad.

L'outil ecodiag (\url{https://ecoinfo.cnrs.fr/ecodiag-calcul/}) évalue l'impact de la production + transport pour nos serveurs.

\begin{table}[htbp]
\caption{\label{tab-co2e-serveurs}Consommation moyenne annuelle des serveurs physiques, hébergement compris}
\centering
\begin{tabular}{rrrrr}
Année & kWh & CO2e consommation & CO2e production & Total CO2e\\
\hline
2021 & 8323 & 1258.4376 & 743 & 2001.4376\\
2022 & 4118 & 622.6416 & 557 & 1179.6416\\
2023 & 4258 & 643.8096 & 557 & 1200.8096\\
2024 & 4372 & 661.0464 & 472 & 1133.0464\\
\end{tabular}
\end{table}
\begin{center}
\begin{tabular}{rrr}
2024 & 4372 & 472\\
\end{tabular}
\end{center}
\end{block}
\begin{block}{Serveurs virtuels}
WINTER est composé d'un ensemble de serveurs physiques hébergés dans plusieurs data centres au PUE moyen de \texttt{1.4}. La part de consommation imputée à Résif-DC est évaluée par la proportion de facturation.

\begin{table}[htbp]
\label{tab-co2e-virt}
\centering
\begin{tabular}{rrr}
Année & Nombre de VM & eq CO\textsubscript{2} (kg)\\
\hline
2021 & 18 & 302.4\\
2022 & 25 & 420.\\
2023 & 23 & 386.4\\
2024 & 21 & 352.8\\
\end{tabular}
\end{table}
\end{block}
\end{block}
\begin{block}{Transfert de données}
C'est à dire, 1.4g eqCO\textsubscript{2} pour 1Go de données transférées.

\begin{table}[htbp]
\label{tab-co2e-reseau}
\centering
\begin{tabular}{rrr}
year & Volume (GB) & kg CO2e\\
\hline
2021 & 116029.0 & 162\\
2022 & 156074.3 & 219\\
2023 & 101044.4 & 141\\
2024 & 252659.0 & 354\\
\end{tabular}
\end{table}
\end{block}
\begin{block}{Déplacements}
L'équipe des ingénieurs Résif se déplace occasionnellement pour des réunions nationales, européennes ou pour des conférences.

La politique locale est de prendre le moins possible l'avion.

Dans ce calcul, nous essayons de tenir compte des déplacements domicile/travail.

\begin{table}[htbp]
\label{tab-co2e-deplacements}
\centering
\begin{tabular}{rrrr}
year & Transports for work & Transports for meetings & Total\\
\hline
2021 & 2644 & 120 & 2764\\
2022 & 2000 & 13 & 2013\\
2023 & 1843 & 101 & 1944\\
2024 & 1843 & 728 & 2571\\
\end{tabular}
\end{table}
\end{block}
\end{frame}
\end{document}
